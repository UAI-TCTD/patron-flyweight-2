﻿Public Class SistemaDeParticulas
    Implements ISistemaDeParticulas

    Dim gravedad As Double = 0.02F

    Dim particulas As New List(Of ParticulaPesada)

    Sub New(cantidad As Integer, centro As PointF)

        Dim rnd As New Random
        Dim bmp As Image = Bitmap.FromFile(Application.StartupPath & "\particula.png")

        For i As Integer = 1 To cantidad

            Dim par As New ParticulaPesada(New Bitmap(bmp, 5, 5)) 'Cada partícula tiene como estado intrinseco un bitmap
            par.posicion = centro
            par.vel = New PointF(CDbl(rnd.NextDouble() * 5 - 2.5F), CDbl(rnd.NextDouble() * -5))
            particulas.Add(par)

        Next

    End Sub

    'Debería haber metido este código en una clase de arriba

    Public Sub dibujar(g As Graphics) Implements ISistemaDeParticulas.dibujar
        For Each p As ParticulaPesada In particulas
            p.dibujar(g)
        Next
    End Sub

    Public Sub stepear() Implements ISistemaDeParticulas.stepear

        For Each p As ParticulaPesada In particulas

            Dim pos As PointF = p.posicion
            Dim vel As PointF = p.vel
            pos.X += vel.X
            pos.Y += vel.Y 'incremento más el y en el mismo tiempo x la acel.
            p.posicion = pos
            vel.Y += gravedad
            p.vel = vel

        Next

    End Sub

   End Class
