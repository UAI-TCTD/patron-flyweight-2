﻿Public Class ParticulaPesada
    Public Property imagen As Image 'Esto la hace pesada
    Public Property posicion As PointF
    Public Property vel As PointF

    Sub New(imagen As Image)
        _imagen = imagen
    End Sub

    Sub dibujar(g As Graphics)
        g.DrawImage(imagen, posicion)
    End Sub

End Class
