﻿Public Class ParticulaLigeraConcretaCompartida

    Private _imagen As Image

    Public ReadOnly Property imagen() As Image
        Get
            Return _imagen
        End Get

    End Property

    Sub New(imagen As Image)

        _imagen = New Bitmap(imagen, 5, 5)

    End Sub

End Class
