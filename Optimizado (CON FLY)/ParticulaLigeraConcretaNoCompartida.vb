﻿Public Class ParticulaLigeraConcretaNoCompartida

    Public Property posicion As PointF
    Public Property vel As PointF
    Public Property Flyweight As ParticulaLigeraConcretaCompartida

    Sub dibujar(g As Graphics)
        g.DrawImage(Flyweight.imagen, posicion) 'Dibujo la compartida!
    End Sub

End Class
