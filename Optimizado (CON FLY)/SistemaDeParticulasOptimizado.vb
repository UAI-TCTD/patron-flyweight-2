﻿Public Class SistemaDeParticulasOptimizado
    Implements ISistemaDeParticulas

    Dim gravedad As Double = 0.02F


    Dim particulas As New List(Of ParticulaLigeraConcretaNoCompartida)


    Sub New(cantidad As Integer, centro As PointF)

        Dim rnd As New Random


        Dim bmp As Image = Bitmap.FromFile(Application.StartupPath & "\particula.png")


        Dim parFlyweight As New ParticulaLigeraConcretaCompartida(bmp) 'Acá está la magia porque solo creo una imagen


        For i As Integer = 1 To cantidad

            Dim par As New ParticulaLigeraConcretaNoCompartida()

            par.Flyweight = parFlyweight 'apunta a la única imagen
            par.posicion = centro
            par.vel = New PointF(CDbl(rnd.NextDouble() * 5 - 2.5F), CDbl(rnd.NextDouble() * -5))
            particulas.Add(par)

        Next

    End Sub

    Public Sub dibujar(g As Graphics) Implements ISistemaDeParticulas.dibujar
        For Each p As ParticulaLigeraConcretaNoCompartida In particulas
            p.dibujar(g)
        Next

    End Sub

    Public Sub stepear() Implements ISistemaDeParticulas.stepear

        For Each p As ParticulaLigeraConcretaNoCompartida In particulas

            Dim pos As PointF = p.posicion
            Dim vel As PointF = p.vel
            pos.X += vel.X
            pos.Y += vel.Y
            p.posicion = pos
            vel.Y += gravedad
            p.vel = vel

        Next

    End Sub
End Class
