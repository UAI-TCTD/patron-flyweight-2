﻿Public Class Form1

    Dim sistema As ISistemaDeParticulas

    'Compilar, ir a la carpeta bin, abrir el administrador de procesos y correr el exe. Verificar la memoria ram que consume. Luego poner simulacionOptimizada en True y realizar todo lo anterior. Se va a notar que consume mucha menos RAM porque el estado intrínseco es compartido.
    Dim simulacionOptimizada As Boolean = False

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        SetStyle(ControlStyles.OptimizedDoubleBuffer Or ControlStyles.UserPaint Or ControlStyles.AllPaintingInWmPaint, True)

        If simulacionOptimizada = False Then
            sistema = New SistemaDeParticulas(20000, New PointF(Width / 2, Height / 2))
        Else
            'Con Flyweight
            sistema = New SistemaDeParticulasOptimizado(20000, New PointF(Width / 2, Height / 2))
        End If

        Timer1.Start()

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        sistema.stepear()
        Me.Refresh()
    End Sub

    Private Sub Form1_Paint(sender As Object, e As PaintEventArgs) Handles Me.Paint
        Dim g As Graphics = e.Graphics
        sistema.dibujar(g)
    End Sub
End Class
